package com.anulom.service.anulomuploaddoc;

import androidx.appcompat.app.AppCompatActivity;


import android.content.ComponentName;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class AnulomDashboaed extends AppCompatActivity
{
    Button btnImage,btnPdf,btnList;
    public String userMail,newMailID,documentID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anulom_dashboaed);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null)
        {
            userMail = bundle.getString("EmailID");
            documentID = bundle.getString("documentID");
        }

        btnImage = findViewById(R.id.btn_image);
        btnPdf = findViewById(R.id.btn_pdf);
        btnList = findViewById(R.id.btn_list);

        btnImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AnulomDashboaed.this,HomeActivity.class);
                intent.putExtra("EmailID",userMail);
                intent.putExtra("documentID",documentID);
                startActivity(intent);
            }
        });

        btnPdf.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AnulomDashboaed.this,UploadPdfActivity.class);
                intent.putExtra("EmailID",userMail);
                intent.putExtra("documentID",documentID);
                startActivity(intent);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AnulomDashboaed.this,UpDocumentList.class);
                intent.putExtra("EmailID",userMail);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
       /* Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClassName("com.anulom.executioner", "com.anulom.executioner.UplaodDocumentAnulom.class");
        startActivity(intent);*/

        Intent intent = new Intent(Intent.ACTION_MAIN);
        PackageManager manager = getApplicationContext().getPackageManager();
        intent = manager.getLaunchIntentForPackage("com.anulom.executioner");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        startActivity(intent);
    }
}