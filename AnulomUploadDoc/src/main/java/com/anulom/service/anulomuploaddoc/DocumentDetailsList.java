package com.anulom.service.anulomuploaddoc;

public class DocumentDetailsList
{
    public String executorKey;
    public String documentID;
    public String documentURL;
    public String documentDate;
    public String documentDescription;

    public DocumentDetailsList()
    {
    }

    public DocumentDetailsList(String executorKey, String documentID, String documentURL, String documentDate, String documentDescription) {
        this.executorKey = executorKey;
        this.documentID = documentID;
        this.documentURL = documentURL;
        this.documentDate = documentDate;
        this.documentDescription = documentDescription;
    }

    public String getExecutorKey() {
        return executorKey;
    }

    public void setExecutorKey(String executorKey) {
        this.executorKey = executorKey;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public String getDocumentURL() {
        return documentURL;
    }

    public void setDocumentURL(String documentURL) {
        this.documentURL = documentURL;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentDescription() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription = documentDescription;
    }
}

