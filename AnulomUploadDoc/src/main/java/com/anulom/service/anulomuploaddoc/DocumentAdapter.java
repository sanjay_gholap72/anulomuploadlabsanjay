package com.anulom.service.anulomuploaddoc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.DocumentViewHolder>
{
    private Context mContext;
    private List<DocumentDetailsList> mDataSet;
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();

    private String documentID,documentUrl,documentDate,documentDescription;

    public DocumentAdapter(Context mContext, List<DocumentDetailsList> mDataSet)
    {
        this.mContext = mContext;
        this.mDataSet = mDataSet;
    }

    @NonNull
    @Override
    public DocumentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.document_item,parent, false);
        return new DocumentViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull DocumentViewHolder holder, int position)
    {
        DocumentDetailsList documentDetailsList = mDataSet.get(position);
        documentID = documentDetailsList.getDocumentID();
        documentUrl = documentDetailsList.getDocumentURL();
        documentDate = documentDetailsList.getDocumentDate();
        documentDescription = documentDetailsList.getDocumentDescription();

        holder.txtDocumentId.setText("DocumentID: "+documentID +"\nUploaded date: "+documentDate);
        holder.txtDocumentUrl.setText("DocumentDescription: "+documentDescription);
    }

    @Override
    public int getItemCount()
    {
        return mDataSet.size();

    }


    public class DocumentViewHolder extends RecyclerView.ViewHolder
    {
        TextView txtDocumentId,txtDocumentUrl;
        public DocumentViewHolder(@NonNull View itemView)
        {
            super(itemView);

            txtDocumentId = itemView.findViewById(R.id.txt_id);
            txtDocumentUrl = itemView.findViewById(R.id.txt_url);
        }
    }
}


